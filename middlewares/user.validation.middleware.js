const { user } = require('../models/user');
const createUserValid = (req, res, next) => {

    /*
       TODO: Implement validator for user entity during creation
        —наличие всех полей
        —почта gmail
        —телефон +380..
        —пароль больше 3-х символов, начинается с большой буквы, содержит цифры
     */

    const allFields = Object.keys(user);
    const forbiddenFields = ['id'];

    forbiddenFields.forEach((field) => {
        const index = allFields.indexOf(field);
        index !== -1 &&
        allFields.splice(allFields.indexOf(field), 1);
    });

    for (let field in req.body) {
        if (allFields.indexOf(field) === -1) {
            delete req.body[field];
        }
    }

    const userData = req.body;

    // Наличие всех полей

    if (
        !allFields.every((field) => {
        return userData.hasOwnProperty(field);
    })
    ) {
        res.status(400).send({
            error: true,
            message: 'All fields must be filled in'
        });
        return;
    }

    // Почта gmail

    if (!/^[A-Za-zА-Яа-я0-9][A-Za-zА-Яа-я0-9.]{4,}@gmail\.com$/.test(userData.email)) {
        res.status(400).send({
            error: true,
            message: 'Wrong email format'
        });
        return;
    }

    // Телефон +380..

    if (!/\+380\d{9}$/.test(userData.phoneNumber)) {
        res.status(400).send({
            error: true,
            message: 'Wrong phone number format'
        });
        return;
    }

    // Пароль больше 3-х символов, начинается с большой буквы, содержит цифры

    if (!(/\w{3,}/.test(userData.password) && /\d/.test(userData.password) && /[A-ZА-Я]/.test(userData.password))) {
        res.status(400).send({
            error: true,
            message: 'Wrong password format'
        });
        return;
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validator for user entity during update
    createUserValid(req, res, next);
}

const authValid = (req, res, next) => {
    const userData = req.body;

    if (!userData.email || !userData.password) {
        res.status(400).send({
            error: true,
            message: 'All fields must be filled in'
        });
        return;
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.authValid = authValid;