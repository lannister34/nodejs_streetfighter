const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validator for fighter entity during creation

    const allFields = Object.keys(fighter);
    const forbiddenFields = ['id'];

    forbiddenFields.forEach((field) => {
        const index = allFields.indexOf(field);
        index !== -1 &&
        allFields.splice(allFields.indexOf(field), 1);
    });

    for (let field in req.body) {
        if (allFields.indexOf(field) === -1) {
            delete req.body[field];
        }
    }

    const userData = req.body;

    // Имя

    if (!/^[A-Za-zА-Яа-я]\w+/.test(userData.name)) {
        res.status(400).send({
            error: true,
            message: 'Wrong name format'
        });
        return;
    }

    // Здоровье

    userData.health = req.body.health = Number(req.body.health);

    if (!userData.health || userData.health < 1 || userData.health > 100) {
        res.status(400).send({
            error: true,
            message: 'Wrong health format'
        });
        return;
    }

    userData.health = req.body.health = req.body.health.toFixed(1) > 100 ? 100 : req.body.health.toFixed(1);

    // Сила


    userData.power = req.body.power = Number(req.body.power);

    if (!userData.power || userData.power < 1 || userData.power > 10) {
        res.status(400).send({
            error: true,
            message: 'Wrong power format'
        });
        return;
    }

    userData.power = req.body.power = req.body.power.toFixed(1) > 10 ? 10 : req.body.power.toFixed(1);

    // Блок

    userData.defense = req.body.defense = Number(req.body.defense);

    if (!userData.defense || userData.defense < 1 || userData.defense > 10) {
        res.status(400).send({
            error: true,
            message: 'Wrong defense format'
        });
        return;
    }

    userData.defense = req.body.defense = req.body.defense.toFixed(1) > 10 ? 10 : req.body.defense.toFixed(1);

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validator for fighter entity during update

    createFighterValid(req, res, next);
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;