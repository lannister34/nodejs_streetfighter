const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    isExist(user) {
        const email = this.search({
            'email' : user.email
        });
        if (email) {
            return true;
        } else {
            const phone = this.search({
                'phoneNumber' : user.phoneNumber
            });
            return !!phone;
        }
    }

    create(userData) {
        const user = UserRepository.create(userData);
        if (!user) {
            throw Error('Registration failed');
        }
        return user;
    }

    update(id, userData) {
        const user = UserRepository.update(id, userData);
        if (!user) {
            throw Error('Update failed');
        }
        return user;
    }

    delete(id) {
        return UserRepository.delete(id);
    }

    getAll() {
        return UserRepository.getAll();
    }
}

module.exports = new UserService();