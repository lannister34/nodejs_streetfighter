const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    isExist(fighter) {
        const name = this.search({
            'name' : fighter.name
        });

        return !!name;
    }

    create(fighterData) {
        const fighter = FighterRepository.create(fighterData);
        if (!fighter) {
            throw Error('Registration failed');
        }
        return fighter;
    }

    update(id, fighterData) {
        const fighter = FighterRepository.update(id, fighterData);
        if (!user) {
            throw Error('Update failed');
        }
        return fighter;
    }

    delete(id) {
        return FighterRepository.delete(id);
    }

    getAll() {
        return FighterRepository.getAll();
    }
}

module.exports = new FighterService();