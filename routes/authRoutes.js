const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { authValid } = require('../middlewares/user.validation.middleware');

const router = Router();

router.post('/login', authValid, (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        const userData = req.body;
        const user = AuthService.login(userData);

        res.status(200).send(user);
    } catch (err) {
        res.status(400).send({
            error: true,
            message: 'Email or password is wrong'
        })
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;