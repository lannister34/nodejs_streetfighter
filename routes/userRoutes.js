const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

/*
    TODO: Implement route controllers for user
 */

router.get('/', function(req, res) {
    const users = UserService.getAll();

    users.map((user) => {
        delete user.password;
        delete user.id;

        return user;
    });

    res.status(200).send(users);
});

router.post('/', createUserValid, function(req, res) {
    const userData = req.body;

    if (UserService.isExist(userData)) {
        res.status(400).send({
            'error': true,
            'message' : 'An account with this phone number or email already exists'
        })
    } else {
        try {
            const user = UserService.create(userData);

            res.status(200).send(user);
        } catch(err) {
            res.status(400).send({
                error: true,
                message: err
            });
        }
    }

});

router.get('/:id', function(req, res) {
    const user = UserService.search({'id': req.params.id});

    user ?
        res.status(200).send({
            'email' : user.email,
            'firstName' : user.firstName,
            'lastName' : user.lastName,
            'phoneNumber' : user.phoneNumber,
            'createdAt' : user.createdAt
        }) :
        res.status(404).send({
            error: true,
            message: "User with specified id doesn't exist"
        })
});

router.put('/:id', updateUserValid, function(req, res) {
    try {
        const userData = req.body;
        const user = UserService.update(req.params.id, userData);

        res.status(200).send(user);
    } catch (err) {
        res.status(400).send({
            error: true,
            message: err
        });
    }
});

router.delete('/:id', function(req, res) {
    const deleted = UserService.delete(req.params.id);

    deleted[0] ?
        res.status(200).send() :
        res.status(400).send({
            error: true,
            message: "User with specified id doesn't exist"
        });
});

module.exports = router;