const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', function(req, res) {
    const fighters = FighterService.getAll();

    fighters.map((fighter) => {
        delete fighter.id;

        return fighter;
    });

    res.status(200).send(fighters);
});

router.post('/', createFighterValid, function(req, res) {
    const fighterData = req.body;

    if (FighterService.isExist(fighterData)) {
        res.status(400).send({
            'error': true,
            'message' : 'A Fighter with this name already exists'
        })
    } else {
        try {
            const user = FighterService.create(fighterData);

            res.status(200).send(user);
        } catch(err) {
            res.status(400).send({
                error: true,
                message: err
            });
        }
    }
});

router.get('/:id', function(req, res) {
    const fighter = FighterService.search({'id': req.params.id});

    fighter ?
        res.status(200).send({
            "name": "",
            "health": fighter.health,
            "power": fighter.power,
            "defense": fighter.defense
        }) :
        res.status(404).send({
            error: true,
            message: "Fighter with specified id doesn't exist"
        })
});

router.put('/:id', updateFighterValid, function(req, res) {
    try {
        const fighterData = req.body;
        const fighter = FighterService.update(req.params.id, fighterData);

        res.status(200).send(fighter);
    } catch (err) {
        res.status(400).send({
            error: true,
            message: err
        });
    }
});

router.delete('/:id', function(req, res) {
    const deleted = FighterService.delete(req.params.id);

    deleted[0] ?
        res.status(200).send() :
        res.status(400).send({
            error: true,
            message: "Fighter with specified id doesn't exist"
        });
});

module.exports = router;